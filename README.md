## Skyrim calendar

This project was created for a friend, huge fan of The Elder Scrolls V.

Sharing here so every fan of Skyrim and electronics can wake up every morning enjoying the Skyrim calendar.



I use CLion with the PlatformIO plugin, to flash the code with Arduino IDE will require some tuning.

<img src="./assets/IMG_20210429_150113.jpg" style="zoom:20%;" />

###### Required libraries:

- TwiLiquidCrytal: https://libraries.io/platformio/TwiLiquidCrytal%20by%20Arnakazim
- DS3231: https://libraries.io/github/adafruit/RTClib

###### Hardware:

- ESP8266 ESP-12E
- DS3231
- I2C Serial LCD 16X2
- IIC I2C Logic Level Converter 5V to 3.3V 



If you plan to run it on a battery you may need a converter from the battery voltage to 5V for the display and then a second conversion will be done on the NodeMCU to 3.3V, this will draw a bunch of mA.



###### Notes

> This program is not prepared for low power applications, running on batteries will need an optimisation.

> Use a capacitor to remove the ESP peaks of consumption.

> Removing the LED's on the DS3231 and I2C converter on the Serial LCD will save some power. 

> Getting a battery with a close Voltage to the highest voltage will save some power too.



###### Schema

<img src="./assets/IMG_20210429_162820.jpg" style="zoom:25%;" />



###### API

A new WiFi with the SSID SkyrimCalendar will appear.

For security change the password and use the one that you set to connect to it.

NodeMCU listens on the 192.168.4.1 by default and we specified the port 80.



Available calls:

GET
- http://192.168.4.1/ -> return the possible get requests on plain HTML

- http://192.168.4.1/screen-off/ -> turn off the screen back-light

  response:

  ```json
  {"error": 0, "message":"The screen backlight is off now"}
  ```

  

- http://192.168.4.1/screen-on/ -> turn on the screen back-light

  response:

  ```json
  {"error": 0, "message":"The screen backlight is on now"}
  ```

POST

- http://192.168.4.1/set-hour/ -> sets the hour

  form-data: 

  ```http
  year:2021
  month:4
  day:23
  hour:18
  minutes:57
  seconds:03
  ```

  response on success:

  ```json
  {"error": 0, "message":"The hour was set correctly"}
  ```

  response on failure:

  ```json
  {"error": -2, "message": "POST expected"}
  ```
  ```json
  {"error": -3, "message": "unexpected argument"}
  ```
###### Screen off example
<img src="./assets/IMG_20210429_150149.jpg" style="zoom: 20%;" />



###### Troubleshooting

**The Display just blinks**

- Check that the I2C address is correct, that means the module is not initiating correctly.

- If the I2C address is correct try another I2C Display library like the one from Adafruit. (There are 3 diferent hardware revisions, the library currently in use may be not adequate).



**The display does show nothing**

- Check all the GNDs are correctly wired and that the Level Converter is properly connected
- There are different Level Converters make sure the one you have is not causing problems on the I2C bus.
- Do not connect directly the SDA and SCL from the Display to the NodeMCU that will make the NodeMCU work over the maximum voltage. It may look that it's fine but eventually it will case errors or even burn the NodeMCU
