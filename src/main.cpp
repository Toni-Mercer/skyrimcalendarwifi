/**
 * @author Toni Mercer
 * @license MIT <https://mit-license.org/>
 *
 * This a calendar that displays the date in the skyrim format
 * 
 */
#include <Arduino.h>
#include <Wire.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include "TwiLiquidCrystal.h"
#include "RTClib.h"

// days and moths names
const char *day_names[] = {"Sundas", "Morndas", "Tirdas",  "Middas", "Turdas", "Fredas", "Loredas"};
const char *month_names[] = {"Morning Star", "Sun's Dawn",  "First Seed", "Rain's Hand", "Second Seed", "Mid Year", "Sun's Height", "Last Seed", "Hearthfire",  "Frost Fall", "Sun's Dusk", "Evening Star", "", ""};

const char* ssid     = "SkyrimCalendar";
const char* password = "Dragonborn";

RTC_DS3231 rtc;
const int twiAddress = 0x27; // The I2C address of your LCD backpack
TwiLiquidCrystal lcd(twiAddress);
ESP8266WebServer server(80);


void handleRoot(){
    server.send(200, "text/html", "GET / -> how help<br>POST /set-hour/ -> set the timestamp as a current hour<br>GET /screen-off/ -> turn off backlight<br>GET /screen-on/ -> turn on backlight");
}

void setHour(){
    if (server.method() != HTTP_POST) {
        server.send(405, "application/json", R"({"error": -2, "message": "POST expected"})");
        return;
    } else {
        int argNumber = 0;
        const char *headersAvailable[] = {"year", "month", "day", "hour", "minutes", "seconds"};
        unsigned int year = 2240;
        unsigned int month = 3;
        unsigned int day = 4;
        unsigned int hour = 8;
        unsigned int minutes = 0;
        unsigned int seconds = 0;

        for (int i = 0; i < server.args(); i++) {
            bool found = false;
            for (int j = 0; j < 6; j++) {
                if (server.argName(i) == headersAvailable[j]) {
                    switch (j) {
                        case 0:
                            year = server.arg(i).toInt();
                            found = true;
                            break;
                        case 1:
                            month = server.arg(i).toInt();
                            break;
                        case 2:
                            day = server.arg(i).toInt();
                            found = true;
                            break;
                        case 3:
                            hour = server.arg(i).toInt();
                            found = true;
                            break;
                        case 4:
                            minutes = server.arg(i).toInt();
                            found = true;
                            break;
                        case 5:
                            seconds = server.arg(i).toInt();
                            found = true;
                            break;
                        default:
                            Serial.println("Unexpected argument");
                            server.send(200, "application/json", R"({"error": -3, "message": "unexpected argument"})");
                            return;
                    }
                    if (found) break;
                }
            }
        }

        Serial.println("Now lets set the hour:");
        Serial.print("Year: "); Serial.println(year);
        Serial.print("Month: "); Serial.println(month);
        Serial.print("Day: "); Serial.println(day);
        Serial.print("Hour: "); Serial.println(hour);
        Serial.print("Minutes: "); Serial.println(minutes);
        Serial.print("Seconds: "); Serial.println(seconds);

        String timestampStr = server.arg(argNumber);
        rtc.adjust(DateTime(year, month, day, hour, minutes, seconds));
        server.send(200, "application/json", R"({"error": 0, "message":"The hour was set correctly"})");
        return;
    }
}

void screenOff(){
    lcd.setBacklight(LOW);
    server.send(200, "application/json", R"({"error": 0, "message":"The screen backlight is off now"})");
}

void screenOn(){
    lcd.setBacklight(HIGH);
    server.send(200, "application/json", R"({"error": 0, "message":"The screen backlight is on now"})");
}

void notFound(){
    String message = "File Not Found\n\n";
    message += "URI: ";
    message += server.uri();
    message += "\nMethod: ";
    message += (server.method() == HTTP_GET) ? "GET" : "POST";
    message += "\nArguments: ";
    message += server.args();
    message += "\n";
    for (uint8_t i = 0; i < server.args(); i++) {
        message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
    }
    server.send(404, "text/plain", message);
}

void updateDisplay(){
    DateTime now = rtc.now();

    // lcd
    lcd.clear();
    lcd.setCursor(0, 0); // Set the cursor on the first column and first row.
    lcd.print(day_names[now.dayOfTheWeek()]);
    lcd.print(", ");
    lcd.print(now.day());
    lcd.print(" of"); // this space is to avoid double ff when a day have less letters than the day before
    lcd.setCursor(0, 1); //Set cursor to 2nd column
    lcd.print(month_names[now.month() - 1]);
}

void setup() {
    // Initiate the LCD and turn on the backlight
    lcd.begin(16, 2);           // Initiate the LCD module
    lcd.setBacklight(HIGH);     // Turn on the backlight

    Serial.begin(115200);
    WiFi.softAP(ssid, password);

    // start I2C communication
    Wire.begin();

    if (!rtc.begin()) {
        Serial.println("Couldn't find RTC");
        Serial.flush();
        abort();
    }

    Serial.println();
    Serial.print("Server IP address: ");
    Serial.println(WiFi.softAPIP());
    Serial.print("Server MAC address: ");
    Serial.println(WiFi.softAPmacAddress());

    server.on("/", handleRoot);
    server.on("/set-hour/", setHour);
    server.on("/screen-off/", screenOff);
    server.on("/screen-on/", screenOn);
    server.onNotFound(notFound);
    server.begin();

    // set the schedule compilation time to the rtc
    //  rtc.adjust(DateTime(F(__DATE__), F(__TIME__))); // set the time to the sketch compilation day
}

void loop() {
    updateDisplay();
    server.handleClient();
    delay(1400);
}
